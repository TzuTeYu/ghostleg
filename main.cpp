#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>

#include <SDL2/SDL.h>

#include "tzute/sdl/resources.hpp"
#include "tzute/game/gamestatus.hpp"
#include "tzute/game/ghostleg.hpp"

#define VIEW_HEIGHT		480
#define VIEW_WIDTH		640

#define APPEND_WIDTH	100
#define APPEND_HEIGHT	100

#define WINDOW_HEIGHT	(VIEW_HEIGHT + APPEND_HEIGHT * 2)
#define WINDOW_WIDTH	(VIEW_WIDTH + APPEND_WIDTH * 2)
#define GOAL_LENGTH		(WINDOW_HEIGHT * 10)
#define NUM_PLAYERS		10

static void add_legs(tzute::game::GameStatus &game_status, int goal_length, int num_player, int legs);


int
main(int argc, char *argv[])
{
	tzute::sdl::SDL sdl;
	tzute::sdl::TTFEnv ttfenv;

	tzute::sdl::Window window("Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

	tzute::sdl::Renderer renderer(window, -1, SDL_RENDERER_ACCELERATED);
	renderer.SetOffset(APPEND_WIDTH, APPEND_HEIGHT);

	tzute::game::GameStatus game_status(NUM_PLAYERS, GOAL_LENGTH);
	tzute::game::GhostLeg ghost_leg(game_status, VIEW_WIDTH, VIEW_HEIGHT, APPEND_WIDTH, APPEND_HEIGHT);
	ghost_leg.prepareTexture(renderer);

	add_legs(game_status, GOAL_LENGTH, NUM_PLAYERS, 100);

	bool quit = false;

	while (!quit && !game_status.end()) {
		SDL_Event e;

		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
				case SDLK_UP:
					game_status.add_user_leg(0, -1);
					break;
				case SDLK_DOWN:
					game_status.add_user_leg(0, 1);
				}
			}
		}

		SDL_Rect rect = {0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};
		renderer.SetOffset(0, 0);
		renderer.SetDrawColor(0xFF, 0xFF, 0xFF, 0xFF);
		renderer.FillRect(&rect);

		ghost_leg.setViewOffset(game_status.get_game_length() - 32);
		ghost_leg.draw(renderer);

		SDL_RenderPresent(renderer);
		SDL_Delay(1000 * 1.0f / 60);

		game_status.step();
	}

	SDL_Delay(3 * 1000);
	return 0;
}

static void
add_legs(tzute::game::GameStatus &game_status, int goal_length, int num_player, int legs)
{
	for (int i = 0; i < legs; ++i) {
		int pos = rand() % goal_length;
		int player1 = rand() % 9;
		int player2 = player1 + 1;

		game_status.add_leg(pos, player1, player2);
	}

	game_status.sort();
}
