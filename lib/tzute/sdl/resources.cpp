#include <stdexcept>

#include "resources.hpp"

tzute::sdl::SDL::SDL(uint32_t flags)
{
	if (SDL_Init(flags) != 0) {
		throw std::logic_error("Init error");
	}
}

tzute::sdl::SDL::~SDL()
{
	SDL_Quit();
}

tzute::sdl::Window::Window(const char *title, int x, int y, int w, int h, uint32_t flags)
{
	SDL_Window *window = SDL_CreateWindow(title, x, y, w, h, flags);
	if (window == nullptr) {
		throw std::logic_error("Create Window error");
	}

	this->window = window;
}

tzute::sdl::Window::~Window()
{
	if (this->window != nullptr) {
		SDL_DestroyWindow(this->window);
	}
}

tzute::sdl::Renderer::Renderer(SDL_Window* window, int index, uint32_t flags)
{
	SDL_Renderer *renderer = SDL_CreateRenderer(window, index, flags);
	if (renderer == nullptr) {
		throw std::logic_error("Create renderer error");
	}

	this->renderer = renderer;
}

tzute::sdl::Renderer::~Renderer()
{
	if (this->renderer != nullptr) {
		SDL_DestroyRenderer(this->renderer);
	}
}
