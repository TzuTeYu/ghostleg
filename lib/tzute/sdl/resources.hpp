#ifndef _TZUTE_SDL_RESOURCES_HPP
#define _TZUTE_SDL_RESOURCES_HPP

#include <cstdint>
#include <stdexcept>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

namespace tzute {
namespace sdl {

class SDL {
public:
	SDL(uint32_t flags = 0);
	virtual ~SDL();
};

class Offset {
public:
	int x = 0;
	int y = 0;
};

class Window {
public:
	Window(const char *title, int x, int y, int w, int h, uint32_t flags);
	virtual ~Window();

	inline operator SDL_Window* () {
		return this->window;
	}
private:
	SDL_Window *window = nullptr;
};

class Renderer {
public:
	Renderer(SDL_Window* window, int index, uint32_t flags);
	~Renderer();

	inline void
	SetOffset(int x, int y) {
		this->offset.x = x;
		this->offset.y = y;
	}

	inline void
	FillRect(const SDL_Rect *rect) {
		SDL_Rect offset_rect = *rect;
		offset_rect.x += this->offset.x;
		offset_rect.y += this->offset.y;

		if (SDL_RenderFillRect(this->renderer, &offset_rect) < 0) {
			throw std::logic_error("Draw fill rect error");
		}
	}

	inline void
	SetDrawColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
		if (SDL_SetRenderDrawColor(this->renderer, r, g, b, a) < 0) {
			throw std::logic_error("Set color error");
		}
	}

	inline void
	DrawLine(int x1, int y1, int x2, int y2)
	{
		if (SDL_RenderDrawLine(this->renderer, x1 + this->offset.x, y1 + this->offset.y, x2 + this->offset.x, y2 + this->offset.y) < 0) {
			throw std::logic_error("Draw line error");
		}
	}

	inline
	void Copy(SDL_Texture *texture, const SDL_Rect* srcrect, const SDL_Rect* dstrect)
	{
		SDL_Rect rect;

		if (dstrect == nullptr) {
			throw std::logic_error("Not support");
		}

		rect = *dstrect;
		rect.x += this->offset.x;
		rect.y += this->offset.y;

		SDL_RenderCopy(this->renderer, texture, srcrect, &rect);
	}

	inline operator SDL_Renderer* () {
		return this->renderer;
	}

private:
	SDL_Renderer *renderer = nullptr;
	Offset offset;
};

class TTFEnv {
public:
	inline
	TTFEnv()
	{
		if (TTF_Init() == -1) {
			throw std::logic_error(TTF_GetError());
		}
	}

	inline
	~TTFEnv()
	{
		TTF_Quit();
	}
};


template<typename Resource, typename Deleter>
class SimpleResourceWrapper {
public:
	inline
	SimpleResourceWrapper(SimpleResourceWrapper &&resource)
	{
		if (this->resource != nullptr) {
			Deleter deleter;

			deleter(this->resource);
		}

		this->resource = resource.resource;
		resource.resource = nullptr;
	}

	inline SimpleResourceWrapper&
	operator = (SimpleResourceWrapper &&resource)
	{
		if (this->resource != nullptr) {
			Deleter deleter;

			deleter(this->resource);
		}

		this->resource = resource.resource;
		resource.resource = nullptr;

		return *this;
	}

	inline
	operator Resource* () {
		return this->resource;
	}

	SimpleResourceWrapper(const SimpleResourceWrapper &resource) = delete;
	SimpleResourceWrapper& operator = (const SimpleResourceWrapper &resource) = delete;

	inline
	~SimpleResourceWrapper() {
		Deleter deleter;

		deleter(this->resource);
	}
protected:
	inline
	SimpleResourceWrapper() {
	}

protected:
	Resource *resource = nullptr;
};

class TTFFontDeleter {
public:
	inline void
	operator () (TTF_Font *font) {
		TTF_CloseFont(font);
	}
};
class TTFFont : public SimpleResourceWrapper<TTF_Font, TTFFontDeleter> {
public:
	TTFFont(const char *font, int ptsize)
	{
		if ((this->resource = TTF_OpenFont(font, ptsize)) == nullptr) {
			throw std::logic_error(TTF_GetError());
		}
	}

};

class SurfaceDelete {
public:
	inline void
	operator () (SDL_Surface *surface) {
		SDL_FreeSurface(surface);
	}
};
class Surface : public SimpleResourceWrapper<SDL_Surface, SurfaceDelete> {
public:
	inline
	Surface(const char *str, TTF_Font *font, SDL_Color fg)
	{
		if ((this->resource = TTF_RenderUTF8_Blended(font, str, fg)) == nullptr) {
			throw std::logic_error(TTF_GetError());
		}
	}

};

class TextureDelete {
public:
	inline void
	operator () (SDL_Texture *texture) {
		SDL_DestroyTexture(texture);
	}
};
class Texture: public SimpleResourceWrapper<SDL_Texture, TextureDelete> {
public:
	inline
	Texture(SDL_Renderer *renderer, const char *str, TTF_Font *font, SDL_Color fg) {
		Surface surface(str, font, fg);	
		
		if ((this->resource = SDL_CreateTextureFromSurface(renderer, surface)) == nullptr) {
			throw std::logic_error(SDL_GetError());
		}
	}
};

}
}

#endif
