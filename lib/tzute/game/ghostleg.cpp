#include "ghostleg.hpp"

#include <sstream>

namespace tzute {
namespace game {

SDL_Color colors[] = {
	{255, 0, 0 ,0},
	{0, 255, 0, 0},
	{0, 0, 255, 0},
	{255, 255, 0, 0},
	{255, 0, 255, 0},
	{0, 255, 255, 0},
	{125, 0 ,0},
	{0, 125, 0, 0},
	{0, 0, 125, 0},
	{125, 125, 125, 0}
};

GhostLeg::GhostLeg(GameStatus &game_status, int w, int h, int off_x, int off_y): SubWindow(game_status, w, h, off_x, off_y)
{
}

void
GhostLeg::prepareTexture(tzute::sdl::Renderer &renderer)
{
	tzute::sdl::TTFFont font("NotoSans-Regular.ttf", 64);

	this->game_status.traverse_players([&, this] (int index, Player &player) {
		char buffer[64];
		sprintf(buffer, "%d", player.id);

		this->players_texture.push_back(tzute::sdl::Texture(renderer, buffer, font, colors[player.id]));
	});
}

void
GhostLeg::draw(tzute::sdl::Renderer &renderer)
{
	renderer.SetOffset(this->off_x, this->off_y);
	renderer.SetDrawColor(0, 0, 0, 0xFF);

	int show_start = this->view_offset;

	if (show_start < -this->h) {
		show_start = -this->h;
	}
	int	show_end = this->view_offset + this->h;
	if (show_end - game_status.get_goal_length() > this->h) {
		show_end = game_status.get_goal_length() + this->h - 1;
		show_start = game_status.get_goal_length() - 1;
	}

	int bottom_dist = show_start < 0 ? -show_start: 0;
	int top_dist = show_end < game_status.get_goal_length() ? 0 : show_end - game_status.get_goal_length();

	for (int i = 0; i < this->game_status.get_num_players(); ++i) {
		int line_start = top_dist;
		int line_end = this->h - bottom_dist;
		int x = (i + 1) * this->w / this->game_status.get_num_players();

		renderer.DrawLine(x, line_start, x, line_end);
	}

	game_status.traverse(show_start, show_end, [&renderer, this, &show_start] (const tzute::game::Leg &leg) {
		int y = leg.pos - show_start;
		int window_y = (this->h - y);
		int from = (leg.across.first + 1) * this->w / this->game_status.get_num_players();
		int to = (leg.across.second + 1) * this->w / this->game_status.get_num_players();

		renderer.DrawLine(from, window_y, to, window_y);
	}, [&renderer, this, &show_start] (const tzute::game::UserLeg &leg) {
		int y = leg.pos - show_start;
		int window_y = (this->h - y);
		int from = (leg.across.first + 1) * this->w / this->game_status.get_num_players();
		int to = (leg.across.second + 1) * this->w / this->game_status.get_num_players();

		renderer.SetDrawColor(colors[leg.id].r, colors[leg.id].g, colors[leg.id].b, colors[leg.id].a);
		renderer.DrawLine(from, window_y, to, window_y);
	});
	renderer.SetDrawColor(0, 0, 0, 0xFF);

	this->game_status.traverse_players([&, this] (int index, Player &player) {
		tzute::sdl::Texture &texture = this->players_texture[player.id];

		int center_x = (index + 1) * this->w / this->game_status.get_num_players();
		int center_y = this->h - bottom_dist - 32;

		SDL_Rect rect = {center_x - 8, center_y - 32, 16, 64};

		renderer.Copy(texture, NULL, &rect);
	});

}

}
}
