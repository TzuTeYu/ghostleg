#ifndef TZUTE_GAME_SUBWINDOW
#define TZUTE_GAME_SUBWINDOW

#include "tzute/sdl/resources.hpp"
#include "gamestatus.hpp"

namespace tzute {
namespace game {

class SubWindow {
public:
	SubWindow(GameStatus &game_status, int w, int h, int off_x, int off_y);
	virtual void draw(tzute::sdl::Renderer &renderer) = 0;

protected:
	GameStatus &game_status;
	int w;
	int h;
	int off_x;
	int off_y;
};

}
}
#endif
