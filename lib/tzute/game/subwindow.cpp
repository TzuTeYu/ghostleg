#include "subwindow.hpp"

namespace tzute {
namespace game {

SubWindow::SubWindow(GameStatus &game_status, int w, int h, int off_x, int off_y): game_status(game_status)
{
	this->w = w;
	this->h = h;
	this->off_x = off_x;
	this->off_y = off_y;
}

}
}
