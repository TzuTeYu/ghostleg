#ifndef TZUTE_GAME_GHOSTLEG
#define TZUTE_GAME_GHOSTLEG

#include <vector>

#include "tzute/sdl/resources.hpp"

#include "subwindow.hpp"

namespace tzute {
namespace game {

class GhostLeg : public SubWindow {
public:
	GhostLeg(GameStatus &game_status, int w, int h, int off_x, int off_y);
	virtual void draw(tzute::sdl::Renderer &renderer) override;

	void prepareTexture(tzute::sdl::Renderer &renderer);

	inline void setViewOffset(int offset) {
		this->view_offset = offset;
	}

private:
	int view_offset = 0;
	std::vector<tzute::sdl::Texture> players_texture;
};

}
}
#endif
