#ifndef _TZUTE_GAME_GAME_STATUS
#define _TZUTE_GAME_GAME_STATUS

#include <utility>
#include <deque>
#include <vector>
#include <set>
#include <algorithm>
#include <cstdint>


namespace tzute {
namespace game {

class GameStatus;
class Leg {
public:
	int pos;
	std::pair<int, int> across;
};

class UserLeg: public Leg {
public:
	int id;
};

class Player {
public:
	inline Player(int id, int score)
	{
		this->id = id;
		this->score = score;
	}

public:
	int id = 0;
	int score = 0;
};

class GameStatus {
public:
	inline
	GameStatus(const int num_players, int goal_length): num_players(num_players), goal_length(goal_length)
	{
		this->players.reserve(num_players);
		for (int i = 0; i < num_players; ++i) {
			Player player(i, 0);
			this->players.push_back(player);
		}
		this->players.shrink_to_fit();
	}

	inline void
	add_leg(int pos, int across_1, int across_2)
	{
		Leg leg;
		leg.pos = pos;
		leg.across.first = across_1;
		leg.across.second = across_2;

		this->legs.push_back(leg);
	}

	inline void
	add_user_leg(int player_id, int dir)
	{
		int i = 0;
		int end = this->players.size();

		UserLeg leg;

		for (i = 0; i < end; ++i) {
			Player &player = this->players[i];
			if (player.id == player_id) {
				leg.across.first = i;
				leg.across.second = i + dir;

				if (leg.across.second < 0 || leg.across.second >= this->num_players) {
					return;
				}
				break;
			}
		}

		if (i == end) {
			throw std::logic_error("No user found");
		}

		leg.id = player_id;
		leg.pos = this->game_length + 5;
		this->user_legs.insert(leg);
	}

	inline void
	step()
	{
		this->game_length++;

		auto swapper = [this] (const Leg &leg) {
			for (int i = 0, end = this->players.size() - 1; i < end; ++i) {
				std::swap(this->players[leg.across.first], this->players[leg.across.second]);
			}
		};

		this->traverse(this->game_length, this->game_length, swapper, swapper);
	}

	inline bool
	end()
	{
		return this->game_length >= this->goal_length;
	}

	inline int
	get_game_length() {
		return this->game_length;
	}

	inline int
	get_goal_length() {
		return this->goal_length;
	}

	inline int
	get_num_players() {
		return this->num_players;
	}

	inline int
	get_remain_length() {
		return this->goal_length - this->game_length;
	}

	template<typename T, typename U>
	inline void traverse(int beg, int end, const T &t, const U &u) {
		for (auto &leg: this->legs) {
			if (leg.pos < beg) {
				continue;
			}
			if (leg.pos > end) {
				break;
			}
			t(leg);
		}
		for (auto &leg: this->user_legs) {
			if (leg.pos < beg) {
				continue;
			}
			if (leg.pos > end) {
				break;
			}
			u(leg);
		}
	}

	template<typename T>
	inline void traverse_players(const T &t) {
		int i = 0;
		for (auto &player: this->players) {
			t(i, player);
			i++;
		}
	}

	inline void
	sort() {
		std::sort(legs.begin(), legs.end(), [] (const Leg &a, const Leg &b) -> bool {
			return a.pos < b.pos;
		});
	}

private:
	class UserLegCompare {
	public:
		inline bool
		operator () (const UserLeg &v1, const UserLeg &v2)
		{
			if (v1.pos < v2.pos) {
				return true;
			}
			else if (v1.pos > v2.pos) {
				return false;
			}
			else {
				return v1.id == v2.id;
			}
		}
	};

private:
	int game_length = 0;

	const int num_players;
	const int goal_length;

	std::vector<Leg> legs;
	std::set<UserLeg, UserLegCompare> user_legs;
	std::vector<Player> players;
};

}
}
#endif
